%MATLAB 2020b
%name: spring
%author: blazej_marszalek
%date: 12.12.2020
%version: 1.0

r = 0.002116; %[m] radius of a section of a wire
n = 40; %number of coils
m = 0.1; %[kg] mass hung on a spring
tau = 10; %[s] time constant of damping
g = 9.81; %[m/s^2] acceleration of gravity
G = 8E10; %Pa stiffness modulus of steel
R = 0.025; %m radius of spring
 
k = (G*r^4)/(4*n*R^3); %[N/m] coefficient of elasticity 
omega_0 = sqrt(k/m); %[1/s] eigenfrequency
F = m*g; %[N] force causing forced oscillations
beta = 1/(2*tau); %[1/s] damping factor

%calculate and display values of forced oscillations amplitude in
%dependency on frequency for specified value R
omega_M = [];
for f = 1:100:1001
    omega = 2*pi*f;
    omega_M = [omega_M, omega];
    A = F/(m*sqrt((omega_0^2 - omega^2)^2 + 4*beta^2*omega^2)); %dependence of amplitude of t
    X = ['for f  = ' num2str(f) ' [Hz] ' ' amplitude is: ' num2str(A) ' [m]']; 
    disp(X)           
end

%calculate values of eigenfrequency for various radiuses of spring
omega_0_M = [];
for R1 = 0.005:0.002:0.051
    k1 = (G*r^4)/(4*n*R1^3);
    omega_0 = sqrt(k1/m);
    omega_0_M = [omega_0_M, omega_0];
end

%count and display forced oscillations amplitude depending on values of
%frequency of forced oscillations for a few values of eigenfrequency
f_M = [1:5:51]
omega_0_1 = omega_0_M(1);
A1_M = [];
for f = f_M(1:length(f_M))
    omega = 2*pi*f;
    A1 = F/(m*sqrt((omega_0_1^2 + omega^2)^2 + 4*beta^2*omega^2));
    A1_M = [A1_M, A1];
    Y = ['for omega_0  = ' num2str(omega_0_1) '[1/s]  for f  = ' num2str(f) ' [Hz] amplitude is: ' num2str(A1) ' [m]'];
    disp(Y)
end
       
omega_0_2 = omega_0_M(5);
A2_M = [];
for f = f_M(1:length(f_M))
    omega = 2*pi*f;
    A2 = F/(m*sqrt((omega_0_2^2 + omega^2)^2 + 4*beta^2*omega^2));
    A2_M = [A2_M, A2];
    Y = ['for omega_0  = ' num2str(omega_0_2) '[1/s] for f  = ' num2str(f) ' [Hz] amplitude is: ' num2str(A2) ' [m]'];    
    disp(Y)
end
   
omega_0_3 = omega_0_M(11);
A3_M = [];
for f = f_M(1:length(f_M))
    omega = 2*pi*f;
    A3 = F/(m*sqrt((omega_0_3^2 + omega^2)^2 + 4*beta^2*omega^2));
    A3_M = [A3_M, A3];
    Y = ['for omega_0  = ' num2str(omega_0_3) '[1/s] for f  = ' num2str(f) ' [Hz] amplitude is: ' num2str(A3) ' [m]'];    
    disp(Y)
end
   
%create a plot 
subplot(2,1,1);
plot(f_M, A1_M, 'b', f_M, A2_M, 'g', f_M, A3_M, 'r');
title('A(f) blue: R=0.005, green: R=0,015, red: R=0,05  [m]')
xlabel('frequency of forced oscillations [Hz]')
ylabel('aplitude of forced oscillations [m]')  

R_M = [0.005:0.002:0.051]
omega_r_M = [];

for omega_0 = omega_0_M(1:length(omega_0_M)) 
    omega_r = sqrt(omega_0^2 - 2*beta^2);
    omega_r_M = [omega_r_M, omega_r];
end

A4_M = [];
for omega_r = omega_r_M(1:length(omega_r_M))
    A4 = F/(m*sqrt((omega_0_3^2 + omega_r^2)^2 + 4*beta^2*omega_r^2));
    A4_M = [A4_M, A4]; 
end

subplot(2,1,2);
plot(R_M, A4_M);
title('A(R)');
xlabel('radius of spring [m]');
ylabel('aplitude od resonance [m]') ;

%save it as pdf
fig = gcf
saveas(fig, 'resonance.pdf');

%writin data into dat file
M = [R_M', A4_M', omega_r_M'];
writematrix(M, 'resonance_analysis_results.dat');
